package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Cartas {

    private String copas;
    private String paus;
    private String ouro;
    private String espada;
    private List<String> cartas = new ArrayList();

    public void setCartas() {

        for (int i = 0; i < 13; i++) {

            if (i == 0) {
                cartas.add("Ás de Copas");
            }
            if (i == 10) {
                cartas.add("Valete de Copas");
            }
            if (i == 11) {
                cartas.add("Dama de Copas");
            }
            if (i == 12) {
                cartas.add("Rei de Copas");
            }
            if (i != 0 && i!= 10 && i!=11 && i!= 12){
                cartas.add(i + 1 + " Copas");
            }
        }

        for (int i = 0; i < 13; i++) {

            if (i == 0) {
                cartas.add("Ás de Paus");
            }
            if (i == 10) {
                cartas.add("Valete de Paus");
            }
            if (i == 11) {
                cartas.add("Dama de Paus");
            }
            if (i == 12) {
                cartas.add("Rei de Paus");
            }
            if (i != 0 && i!= 10 && i!=11 && i!= 12){
                cartas.add(i + 1 + " Paus");
            }
        }

        for(int i = 0; i< 13;i++) {
            if (i == 0) {
                cartas.add("Ás de Ouro");
            }
            if (i == 10) {
                cartas.add("Valete de Ouro");
            }
            if (i == 11) {
                cartas.add("Dama de Ouro");
            }
            if (i == 12) {
                cartas.add("Rei de Ouro");
            }
            if (i != 0 && i!= 10 && i!=11 && i!= 12){
                cartas.add(i + 1 + " Ouro");
            }
        }

        for(int i = 0; i< 13;i++)    {
            if (i == 0) {
                cartas.add("Ás de Espadas");
            }
            if (i == 10) {
                cartas.add("Valete de Espadas");
            }
            if (i == 11) {
                cartas.add("Dama de Espadas");
            }
            if (i == 12) {
                cartas.add("Rei de Espadas");
            }
            if (i != 0 && i!= 10 && i!=11 && i!= 12){
                cartas.add(i + 1 + " Espadas");
            }
        }
    }

    public List<String> getCartas() {
        return cartas;
    }
}
